# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 2.1.1

- patch: (13bc123) Fix build

## 2.1.0

- minor: (2b58938) Merged in target-instance-scan-support (pull request #8)

Target instance scan support

## 2.0.3

- patch: (f71f0c2) Merged in r-support (pull request #7)

R support

## 2.0.2

- patch: (4bd3db2) Enforcing overall grant of permission on rust binaries folder

## 2.0.1

- patch: (7f45a9a) Added C++ example project

- Triggering new build to update Conan within meterian-scan image following
  Let's Encrypt root certificate update

## 2.0.0

- major: (cc6f2dd) Merged in parent-image-update-and-conan-support (pull request #6)

Applied updates to support new Alpine-based parent image and its new tools

## 1.0.9

- patch: (23aef5d) Merged in rust_elixir_and_perl_update (pull request #5)

Rust elixir and perl update

## 1.0.8

- patch: (daca90a) Merged in pipe-dockerhub-image-update (pull request #4)

Updated scripts

Approved-by: Bruno Bossola

## 1.0.7

- patch: (351ae98) Merged update into master

## 1.0.6

- patch: (1a42c7e) Merged in go-bitbucket-hosted-private-module-update (pull request #3)

Now also supporting private go modules hosted on BitBucket and GitLab

## 1.0.5

- patch: (bf29306) Merged in pipe-update (pull request #2)

Pipe update

## 1.0.4

- patch: Now pointing at the correct docker image in the pipe.yml

## 1.0.3

- patch: Renamed the pipe as part of BitBucket Official pipes submission

## 1.0.2

- patch: Updated the pipe to support the usage of the WORKDIR parameter

## 1.0.1

- patch: Now pointing to correct client path within the container

## 1.0.0

- major: Major update for the Meterian bitbucket pipe

## 0.1.0

- minor: Initial release
