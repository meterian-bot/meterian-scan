#!/usr/bin/env bash

RELEASE_VERSION="$(grep "\-pipe:" ../pipe.yml | awk -F ":" '{print $3}')"
set -x 
cd .. \
   && docker build -t \
             meterianbot/bitbucket-meterian-pipe:${RELEASE_VERSION} . \
             || true && cd -
set +x 