RELEASE_VERSION="$(grep "\-pipe:" ../pipe.yml | awk -F ":" '{print $3}')"

echo "Running image meterianbot/bitbucket-meterian-pipe:${RELEASE_VERSION}"

cleanup() {
	rm -fr ClientOfMutabilityDetector || true
	rm -fr bitbucket-pipelines-cli.jar || true
	rm -fr version.txt || true
}

cleanup

git clone --depth=1 git@bitbucket.org:meterian-bot/ClientOfMutabilityDetector.git

if [[ "${DEBUG}" = "true" ]]; then
	docker run \
	    --interactive \
	    --tty \
	    --entrypoint /bin/bash \
	    -e WORKSPACE=$(pwd)/ClientOfMutabilityDetector \
	    -e METERIAN_API_TOKEN=${METERIAN_API_TOKEN} \
	    -e METERIAN_BITBUCKET_USER=${METERIAN_BITBUCKET_USER} \
	    -e METERIAN_BITBUCKET_APP_PASSWORD=${METERIAN_BITBUCKET_APP_PASSWORD} \
	    -e METERIAN_BITBUCKET_EMAIL=${METERIAN_BITBUCKET_EMAIL} \
	    -v $(pwd):$(pwd) \
	    -w $(pwd) \
	    meterianbot/bitbucket-meterian-pipe:${RELEASE_VERSION} || true
else
	docker run \
	    -e WORKSPACE=$(pwd)/ClientOfMutabilityDetector \
	    -e METERIAN_API_TOKEN=${METERIAN_API_TOKEN} \
	    -e METERIAN_BITBUCKET_USER=${METERIAN_BITBUCKET_USER} \
	    -e METERIAN_BITBUCKET_APP_PASSWORD=${METERIAN_BITBUCKET_APP_PASSWORD} \
	    -e METERIAN_BITBUCKET_EMAIL=${METERIAN_BITBUCKET_EMAIL} \
	    -v $(pwd):$(pwd) \
	    -w $(pwd) \
	    meterianbot/bitbucket-meterian-pipe:${RELEASE_VERSION} || true
fi

cleanup