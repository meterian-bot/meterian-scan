#!/bin/bash
#
# Use the Meterian scanner on your project to scan for vulnerabilities using this Bitbucket Pipe in your Pipeline configuration
export ORIGINAL_PATH=$PATH

cp "$(dirname "$0")/common.sh" /tmp
source "/tmp/common.sh"

# rust-specifics
chmod -R 777 /opt/rust/

info "Executing the pipe..."

DEBUG=${DEBUG:="false"}
enable_debug() {
  if [[ "${DEBUG}" == "true" ]]; then
    info "Enabling debug mode."
    set -x
  fi
}
enable_debug

groupadd meterian
useradd -g meterian meterian -d /home/meterian

# creating home dir if it doesn't exist
if [[ ! -d "/home/meterian" ]];
then
    mkdir /home/meterian
fi

# changing home dir group and ownership
chown meterian:meterian /home/meterian

# launch meterian client 
set +e
if [[ "${DEBUG}" == "true" ]];then
  su meterian -c -m /meterian.sh
else
  su meterian -c -m /meterian.sh 2> /dev/null
fi
status=$?
set -e

if [[ "${status}" == "0" ]]; then
  success "Success!"
else
  fail "Error! Breaking build."
  exit "${status}"
fi