# Bitbucket Pipelines Pipe: Meterian Scan

Scan for vulnerabilities in your projects with the Meterian scanner Bitbucket Pipe by adding it to your Pipeline configuration.
A list of supported project languages can be found [here](https://docs.meterian.io/languages-support/languages-intro).

## YAML Definition

Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:

```yaml
script:
  - pipe: meterian-bot/meterian-scan:2.1.1
    variables:
      METERIAN_API_TOKEN: "<string>"
      # WORKSPACE: "<string>" # Optional 
      # METERIAN_CLIENT_ARGS: "<string>" # Optional
      # DEBUG: "<boolean>" # Optional

```
## Variables

### General variables

| Variable              | Usage                                                       |
| --------------------- | ----------------------------------------------------------- |
| METERIAN_API_TOKEN (*)(B)              | A token to be able to invoke the Meterian scanner client.  |
| WORKSPACE                  | Work folder where the project resides. Defaults to "."  |
| METERIAN_CLIENT_ARGS       | Extra program arguments passed to the Meterian scanner client. For information on additional client arguments, see [manual](https://www.meterian.io/documents/meterian-cli-manual.pdf) or [download the meterian Java client](https://www.meterian.io/downloads/meterian-cli.jar) and run `java -jar meterian-cli.jar --help` |
| DEBUG                 | Turn on extra debug information. Default: `false`. |
| BP_GITHUB_USER (GH) | You GitHub username |
| BP_GITHUB_TOKEN (GH)(B) | Valid [GitHub access token](https://docs.github.com/en/github/authenticating-to-github/creating-a-personal-access-token) |
| BP_BITBUCKET_USER (BB) | Your BitBucket username |
| BP_BITBUCKET_APP_PASSWORD (BB)(B) | Valid [BitBucket application password](https://confluence.atlassian.com/bitbucket/app-passwords-828781300.html) |
| BP_GITLAB_USER (GL) | Your GitLab username |
| BP_GITLAB_TOKEN (GL)(B) | Valid [GitLab access token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html) |
| GOPRIVATE | Comma-separated list of glob patterns ([in the syntax of Go's path.Match](https://golang.org/pkg/path/filepath/#Match)) of module path prefixes. Find out more [here](https://golang.org/cmd/go/#hdr-Module_configuration_for_non_public_modules) |
| METERIAN_ENV | The subdomain of your site where your instance of Meterian runs (given the instance of Meterian running on https://www.meterian.io this would be set to www) |
| METERIAN_DOMAIN | The domain of your site (given the instance of Meterian running on https://www.meterian.io this would be set to meterian.io) |
| METERIAN_PROTO | The HTTP protocol of your site (when unset the default is https) (given the instance of Meterian running on https://www.meterian.io this would be set to https) |

_(*) = required variable._
_(B) = add these variables as secured environment variables in Bitbucket under Account Variables ( `<your account> > Settings > Pipelines > Account Variables` )_
_(GH) = this variable is part of a username/password set of variables and should always be provided to allow authentication when GitHub-hosted private Go modules are in your project_.
_(BB) = this variable is part of a username/password set of variables and should always be provided to allow authentication when BitBucket-hosted private Go modules are in your project_.
_(GL) = this variable is part of a username/password set of variables and should always be provided to allow authentication when GitLab-hosted private Go modules are in your project_.

## Prerequisites

You would need the below done before you can start using Meterian Scan:

- Bitbucket account: if you do not already have one, see http://bitbucket.org
- Meterian API Token: if you do not already have one, register at http://meterian.io. You can create a token via the Meterian Dashboard to run analysis using the Meterian client.
- Setup necessary secure environment variables in your Bitbucket account under Account Variables ( `<your account> > Settings > Pipelines > Account variables` ), also check out [Secured environment variables](https://confluence.atlassian.com/bitbucket/variables-in-pipelines-794502608.html#Variablesinpipelines-Securedvariables)
- Enable pipelines in your project repository: ( `Your repo Settings > Pipelines section > Settings > Enable pipelines` )

## Examples

**Basic example**:

```yaml
script:
  - pipe: meterian-bot/meterian-scan:2.1.1
    variables:
      METERIAN_API_TOKEN: $METERIAN_API_TOKEN
```

__Advanced examples__:

```yaml
script:
  - pipe: meterian-bot/meterian-scan:2.1.1
    variables:
      METERIAN_API_TOKEN: $METERIAN_API_TOKEN
      METERIAN_CLIENT_ARGS: "--min-security=90"
      DEBUG: "true"
```

```yaml
script:
  - pipe: meterian-bot/meterian-scan:2.1.1
    variables:
      METERIAN_API_TOKEN: $METERIAN_API_TOKEN
      METERIAN_CLIENT_ARGS: "--min-security=90"
      DEBUG: "true"
```

Setup for scanning a Go project with private modules hosted on GitHub
```yaml
script:
  - pipe: meterian-bot/meterian-scan:2.1.1
    variables:
      METERIAN_API_TOKEN: $METERIAN_API_TOKEN
      BP_GITHUB_USER: joe-bloggs123
      BP_GITHUB_TOKEN: $BP_GITHUB_TOKEN
      GOPRIVATE: "github.com/MyOrgName"
```
Setup for scanning a Go project with private modules hosted on GitHub and BitBucket
```yaml
script:
  - pipe: meterian-bot/meterian-scan:2.1.1
    variables:
      METERIAN_API_TOKEN: $METERIAN_API_TOKEN
      BP_GITHUB_USER: joe-bloggs123
      BP_GITHUB_TOKEN: $BP_GITHUB_TOKEN
      BP_BITBUCKET_USER: john-doe456
      BP_BITBUCKET_APP_PASSWORD: $BP_BITBUCKET_APP_PASSWORD
      GOPRIVATE: "github.com/MyOrgName,bitbucket.org/MyOrgName"
```
Setup for performing a scan that will target your dedicated instance of Meterian
```yaml
script:
  - pipe: meterian-bot/meterian-scan:2.1.1
    variables:
      METERIAN_API_TOKEN: $METERIAN_API_TOKEN
      METERIAN_ENV: acme
      METERIAN_DOMAIN: mycompany.uk
      METERIAN_PROTO: https
```


The above `GOPRIVATE` variable causes the go command to treat as private any module with a path prefix matching the provided pattern (e.g. github.com/MyOrgName/xyzzy or bitbucket.org/MyOrgName/xyzzy)

## Example projects

- [Java sample project](https://bitbucket.org/meterian-bot/java-sample-project/src/master/bitbucket-pipelines.yml)
- [Go sample project](https://bitbucket.org/meterian-bot/go-sample-project/src/master/bitbucket-pipelines.yml)
- [Swift sample project](https://bitbucket.org/meterian-bot/swift-sample-project/src/master/bitbucket-pipelines.yml)
- [Dotnet project](https://bitbucket.org/meterian-bot/dotnet-sample-project/src/master/bitbucket-pipelines.yml)
- [Php sample project](https://bitbucket.org/meterian-bot/php-sample-project/src/master/bitbucket-pipelines.yml)
- [Ruby sample project](https://bitbucket.org/meterian-bot/ruby-sample-project/src/master/bitbucket-pipelines.yml)
- [Python sample project](https://bitbucket.org/meterian-bot/python-sample-project/src/master/bitbucket-pipelines.yml)
- [Node sample project](https://bitbucket.org/meterian-bot/node-sample-project/src/master/bitbucket-pipelines.yml)
- [Scala sample project](https://bitbucket.org/meterian-bot/scala-sample-project/src/master/bitbucket-pipelines.yml)
- [Rust sample project](https://bitbucket.org/meterian-bot/rust-sample-project/src/master/bitbucket-pipelines.yml)
- [Elixir sample project](https://bitbucket.org/meterian-bot/elixir-sample-project/src/master/bitbucket-pipelines.yml)
- [Perl sample project](https://bitbucket.org/meterian-bot/perl-sample-project/src/master/bitbucket-pipelines.yml)
- [C++ sample project](https://bitbucket.org/meterian-bot/cpp-sample-project/src/master/bitbucket-pipelines.yml)
- [R sample project](https://bitbucket.org/meterian-bot/r-sample-project/src/master/bitbucket-pipelines.yml)

## Support

If you’d like help with this pipe, or you have an issue or feature request, please get in touch using our [support channel](mailto:support@meterian.com).

If you’re reporting an issue, please include:

- the version of the pipe
- relevant logs and error messages
- steps to reproduce

## Scan Outcome

Upon scanning a given project, an analysis of the dependencies found is performed and the outcome is shown in the form of scores. Namely vulnerability, stability and licensing scores.

```bash
Final results: 
- security:     99     (minimum: 90)
- stability:    100    (minimum: 90)
- licensing:    98     (minimum: 90)
```
Please note that when the final scores are below the set thresholds your pipeline workflow will be interrupted preventing you to potentially ship vulnerable code.

Registered Meterian users can modify the score thresholds from the personal dashboard (to register refer to the [Prerequisites](#Prerequisites) section for more information).

## License

See [LICENSE.txt](LICENSE.txt) for further details.